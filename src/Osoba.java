public class Osoba {

    public Osoba(String ime) {
        this.ime = ime;
    }

    public Osoba() {
    }

    private String ime;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }


}
